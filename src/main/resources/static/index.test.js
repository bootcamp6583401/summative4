global.fetch = jest.fn((url, options) => {
  const method = options?.method ? options.method : "GET";
  const base = "http://localhost:8000/";
  const responseMapping = {
    GET: Promise.resolve({
      json: () =>
        Promise.resolve([
          {
            id: 1,
            name: "name",
            email: "email",
            lastName: "body",
            departmentName: "departmetn",
            birthDate: "2023-01-01",
          },
        ]),
    }),
    POST: Promise.resolve({
      json: () =>
        Promise.resolve({
          id: 1,
          name: "name",
          email: "email",
          lastName: "body",
          departmentName: "departmetn",
          birthDate: "2023-01-01",
        }),
    }),
    PUT: Promise.resolve({
      json: () =>
        Promise.resolve({
          id: 1,
          name: "name",
          email: "email",
          lastName: "body",
          departmentName: "departmetn",
          birthDate: "2023-01-01",
        }),
    }),
    DELETE: Promise.resolve({
      json: () => Promise.resolve({}),
    }),
  };

  res = responseMapping[method];
  if (url.includes("many"))
    return Promise.resolve({
      json: () =>
        Promise.resolve([
          {
            id: 1,
            name: "name",
            email: "email",
            lastName: "body",
            departmentName: "departmetn",
            birthDate: "2023-01-01",
          },
        ]),
    });
  return res;
});

beforeEach(() => {
  fetch.mockClear();
});

const baseHTML = `<h1 class="mt-8 text-4xl text-white font-bold text-center">
User Management
</h1>
<div class="w-[900px] mx-auto my-16">
<table class="table-auto w-full border-collapse bg-white shadow-md">
  <thead class="text-xl bg-blue-950 text-white">
    <tr class="font-semibold">
      <th class="table-cell">Name</th>
      <th class="table-cell">Last Name</th>
      <th class="table-cell">Email</th>
      <th class="table-cell">Birthdate</th>
      <th class="table-cell">Department</th>
      <th class="table-cell">Actions</th>
    </tr>
  </thead>
  <tbody id="value-table-body"></tbody>
</table>
<button
  class="bg-blue-950 text-white font-bold px-4 py-2 rounded-md mt-4 text-xl"
  id="bulk-save"
>
  Save
</button>
<div
  class="bg-black/25 fixed inset-0 flex justify-center items-center"
  id="modal"
>
  <div
    class="bg-white w- max-w-full rounded-md shadow-md w-[500px] p-8"
    id="modal__content"
  >
    <div class="mb-4">
      <h1 id="modal__title" class="text-4xl font-bold mb-4">
        Testing title
      </h1>
      <p id="modal__message" class="text-2xl">This is the message</p>
    </div>
    <div class="flex justify-end gap-4" id="modal__btns">
      <button
        id="modal__confirm"
        class="bg-green-600 hover:bg-green-500 rounded-md text-white px-4 py-2"
      >
        Yes
      </button>
      <button
        id="modal__cancel"
        class="bg-red-600 hover:bg-red-500 rounded-md text-white px-4 py-2"
      >
        Cancel
      </button>
    </div>
  </div>
</div>
</div>`;

test("get data from database and display", async () => {
  document.body.innerHTML = baseHTML;
  require("./index");

  const valueTableBody = document.querySelector("#value-table-body");

  const modalBackdrop = document.querySelector("#modal");
  modalBackdrop.classList.add("hidden");
  const modalTitle = document.querySelector("#modal__title");
  const modalMessage = document.querySelector("#modal__message");
  const modalCancel = document.querySelector("#modal__cancel");
  const bulkSaveBtn = document.querySelector("#bulk-save");
  await bulkSaveBtn.click();
  const modalConfirm = document.querySelector("#modal__confirm");

  await new Promise((resolve) => {
    setTimeout(async () => {
      const row = document.querySelector('[data-user-id="1"');

      const deleteBtn = row.querySelector(".danger-round-btn");
      const okBtn = row.querySelector(".green-round-btn");
      await deleteBtn.click();
      await okBtn.click();

      console.log("ROOW", row, deleteBtn);

      expect(Array.from(document.querySelectorAll(".value-row")).length).toBe(
        11
      );
      expect(global.fetch).toHaveBeenCalledTimes(1);
      resolve(true);
    }, 3000);
  });
}, 200000);

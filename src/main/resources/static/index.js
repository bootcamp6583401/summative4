const valueTableBody = document.querySelector("#value-table-body");

const modalBackdrop = document.querySelector("#modal");
modalBackdrop.classList.add("hidden");
const modalTitle = document.querySelector("#modal__title");
const modalMessage = document.querySelector("#modal__message");
const modalCancel = document.querySelector("#modal__cancel");
const modalBtns = document.querySelector("#modal__btns");

const bulkSaveBtn = document.querySelector("#bulk-save");

const modalConfirm = document.querySelector("#modal__confirm");

modalCancel.addEventListener("click", () => {
  modalBackdrop.classList.add("hidden");
});

let currentModalMode = "SAVE";
let selectedRow = null;

bulkSaveBtn.addEventListener("click", () => {
  currentModalMode = "BULK-SAVE";
  showModal(
    "Bulk Save/Upate Users",
    "Are you sure you want to save/update all these users?  "
  );
});

modalConfirm.addEventListener("click", async () => {
  switch (currentModalMode) {
    case "SAVE":
      if (selectedRow) {
        const rowCells = getRowCells(selectedRow);
        await saveIndividualUser({
          name: rowCells.name.value,
          lastName: rowCells.lastName.value,
          email: rowCells.email.value,
          birthDate: rowCells.birthDate.value,
          department: {
            name: rowCells.department.value,
          },
        });
      }
      break;
    case "DELETE":
      console.log("DELETE");
      await deleteIndividualUser(selectedRow.dataset.userId);
      break;
    case "UPDATE":
      if (selectedRow) {
        const rowCells = getRowCells(selectedRow);
        await updateIndividualUser({
          id: selectedRow.dataset.userId,
          name: rowCells.name.value,
          lastName: rowCells.lastName.value,
          email: rowCells.email.value,
          birthDate: rowCells.birthDate.value,
          department: {
            name: rowCells.department.value,
          },
        });
      }
      console.log("UPDATE");
      break;
    case "BULK-SAVE":
      const filledRows = getFilledRows();

      const usersData = filledRows.map((row) => {
        const rowCells = getRowCells(row);
        return {
          id: row.dataset.userId,
          name: rowCells.name.value,
          lastName: rowCells.lastName.value,
          email: rowCells.email.value,
          birthDate: rowCells.birthDate.value,
          department: {
            name: rowCells.department.value,
          },
        };
      });

      await saveBulkUsers(usersData);

      break;

    default:
      console.log("SWAG");
      break;
  }
  modalBackdrop.classList.add("hidden");
  valueTableBody.innerHTML = "";
  await displaySavedUsers();
  handleUpdateRows();
});

function createRowInput(type, className, placeholder) {
  const valueCell = document.createElement("td");
  valueCell.classList.add("table-cell");

  const input = document.createElement("input");
  input.className = className;
  input.type = type;
  input.placeholder = placeholder;
  input.addEventListener("input", handleUpdateRows);
  valueCell.appendChild(input);

  return valueCell;
}

function createRow(name, lastName, email, birthDate, departmentName, id) {
  const valueRow = document.createElement("tr");
  valueRow.classList.add("value-row");

  if (id) valueRow.dataset.userId = id;

  const nameData = createRowInput(
    "text",
    "table-input value-row__name",
    "First Name"
  );
  nameData.querySelector("input").value = name || "";
  const lastNameData = createRowInput(
    "text",
    "table-input value-row__lastName",
    "Last Name"
  );
  lastNameData.querySelector("input").value = lastName || "";
  const birthDateData = createRowInput(
    "date",
    "table-input value-row__birthDate"
  );
  birthDateData.querySelector("input").value = birthDate || "";
  const emailData = createRowInput(
    "email",
    "table-input value-row__email",
    "Email"
  );

  emailData.querySelector("input").value = email || "";

  const departmentData = createRowInput(
    "text",
    "table-input value-row__department",
    "Department"
  );

  departmentData.querySelector("input").value = departmentName || "";
  valueRow.appendChild(nameData);
  valueRow.appendChild(lastNameData);
  valueRow.appendChild(emailData);
  valueRow.appendChild(birthDateData);
  valueRow.appendChild(departmentData);

  const actionCell = document.createElement("td");
  actionCell.classList.add("table-cell");
  actionCell.innerHTML = `
        <div class="flex gap-2 action-container">
            <button class="danger-round-btn">&cross;</button>
            <button class="green-round-btn">&checkmark;</button>
        </div>`;

  const deleteBtn = actionCell.querySelector(".danger-round-btn");
  const saveBtn = actionCell.querySelector(".green-round-btn");

  deleteBtn.addEventListener("click", () => {
    selectedRow = valueRow;
    if (valueRow.dataset.userId) {
      currentModalMode = "DELETE";
      showModal(
        "Are you sure",
        `Are you sure you want to delete user with id ${valueRow.dataset.userId}?`
      );
    } else {
      showModal(
        "Non-existing user",
        "Cannot delete user that doesn't exist.",
        true
      );
    }
  });

  saveBtn.addEventListener("click", () => {
    selectedRow = valueRow;
    currentModalMode = valueRow.dataset.userId ? "UPDATE" : "SAVE";
    showModal(
      "Are you sure",
      valueRow.dataset.userId
        ? `Are you sure you want to update user with id ${valueRow.dataset.userId}?`
        : `Are you sure you want to save this user?`
    );
  });

  valueRow.appendChild(actionCell);

  valueTableBody.appendChild(valueRow);
}

function generateValueRows(n) {
  for (let i = 0; i < n; i++) {
    createRow();
  }
}

async function displaySavedUsers() {
  const savedUsers = await getUsers();

  savedUsers.forEach((u) => {
    createRow(u.name, u.lastName, u.email, u.birthDate, u.departmentName, u.id);
  });
}

(async () => {
  await displaySavedUsers();

  handleUpdateRows();
})();

function handleUpdateRows() {
  const totalFilledRows = getFilledRows().length;

  const totalRows = Array.from(document.querySelectorAll(".value-row")).length;

  generateValueRows(10 - (totalRows - totalFilledRows));
}

function getRowCells(row) {
  const name = row.querySelector(".value-row__name");
  const lastName = row.querySelector(".value-row__lastName");
  const email = row.querySelector(".value-row__email");
  const birthDate = row.querySelector(".value-row__birthDate");
  const department = row.querySelector(".value-row__department");

  return { name, lastName, email, birthDate, department };
}

function getFilledRows() {
  const rows = Array.from(document.querySelectorAll(".value-row"));
  const filledRows = rows.filter((row) => {
    const cells = getRowCells(row);
    return (
      cells.name.value &&
      cells.email.value &&
      cells.birthDate.value &&
      cells.department.value
    );
  });

  return filledRows;
}

function showModal(title, message, noAction) {
  modalBackdrop.classList.remove("hidden");
  modalTitle.textContent = title;
  modalMessage.textContent = message;

  if (noAction) {
    modalConfirm.classList.add("hidden");
  } else {
    modalConfirm.classList.remove("hidden");
  }
}

async function getUsers() {
  const deps = await fetch("http://localhost:8080/users");
  return await deps.json();
}

async function saveIndividualUser(user) {
  const newUser = await fetch("http://localhost:8080/users", {
    method: "POST",
    body: JSON.stringify({
      name: user.name,
      lastName: user.lastName ? user.lastName : undefined,
      email: user.email,
      birthDate: user.birthDate,
      department: {
        name: user.department.name,
      },
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
}

async function updateIndividualUser(user) {
  const newUser = await fetch(`http://localhost:8080/users/${user.id}`, {
    method: "PUT",
    body: JSON.stringify({
      name: user.name,
      lastName: user.lastName ? user.lastName : undefined,
      email: user.email,
      birthDate: user.birthDate,
      department: {
        name: user.department.name,
      },
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
}

async function deleteIndividualUser(id) {
  await fetch(`http://localhost:8080/users/${id}`, {
    method: "DELETE",
  });
}

async function saveBulkUsers(users) {
  const newUsers = await fetch("http://localhost:8080/users/many", {
    method: "POST",
    body: JSON.stringify(
      users.map((user) => {
        return {
          id: user.id,
          name: user.name,
          lastName: user.lastName ? user.lastName : undefined,
          email: user.email,
          birthDate: user.birthDate,
          department: {
            name: user.department.name,
          },
        };
      })
    ),
    headers: {
      "Content-Type": "application/json",
    },
  });
}

package summative.four.summative4.controllers;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.four.summative4.models.Department;
import summative.four.summative4.models.User;
import summative.four.summative4.services.DepartmentService;
import summative.four.summative4.services.UserService;


@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    @Autowired
    UserService service;


    @Autowired
    DepartmentService depService;

    @GetMapping
    public Iterable<User> getAll() {
        return service.getAll();
    }

    @PostMapping
    public User save(@RequestBody User user) {
        String depName = user.getDepartmentName().toLowerCase();
        Optional<Department> depData = depService.findByName(depName);
        user.setDepartment(null);

        if(depData.isPresent()){
            System.out.println("PRESENT");
            user.setDepartment(depData.get());
        }else {
            System.out.println("NEW NEW NEW");
            Department newDep = new Department();
            newDep.setName(depName);
            depService.save(newDep);

            user.setDepartment(newDep);
        }
        return service.save(user);
    }

    @PostMapping("/many")
    public List<User> saveAll(@RequestBody Iterable<User> users) {
        for (User user : users) {
            String depName = user.getDepartmentName().toLowerCase();
            Optional<Department> depData = depService.findByName(depName);
            user.setDepartment(null);

            if(depData.isPresent()){
                System.out.println("PRESENT");
                user.setDepartment(depData.get());
            }else {
                System.out.println("NEW NEW NEW");
                Department newDep = new Department();
                newDep.setName(depName);
                depService.save(newDep);

                user.setDepartment(newDep);
            }
        }
        return service.saveMany(users);
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public User updateById(@PathVariable int id, @RequestBody User user) {
        String depName = user.getDepartmentName().toLowerCase();
        Optional<Department> depData = depService.findByName(depName);
        user.setDepartment(null);

        if(depData.isPresent()){
            user.setDepartment(depData.get());
        }else {
            Department newDep = new Department();
            newDep.setName(depName);
            depService.save(newDep);

            user.setDepartment(newDep);
        }
        return service.updateById(id, user);
    }

}
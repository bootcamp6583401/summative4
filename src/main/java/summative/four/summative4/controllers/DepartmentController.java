package summative.four.summative4.controllers;




import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import summative.four.summative4.models.Department;
import summative.four.summative4.services.DepartmentService;


@RestController
@RequestMapping("/departments")
public class DepartmentController {

    @Autowired
    DepartmentService service;

    @GetMapping
    public Iterable<Department> getAll() {
        return service.getAll();
    }

    @PostMapping
    public Department save(@RequestBody Department product) {
        return service.save(product);
    }

    @PostMapping("/many")
    public List<Department> saveAll(@RequestBody Iterable<Department> users) {
        return service.saveMany(users);
    }

    @GetMapping("/{id}")
    public Department getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Department updateById(@PathVariable int id, @RequestBody Department product) {
        return service.updateById(id, product);
    }

}

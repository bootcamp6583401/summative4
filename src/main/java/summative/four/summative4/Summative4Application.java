package summative.four.summative4;

import org.springframework.aot.generate.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Generated
public class Summative4Application {

	public static void main(String[] args) {
		SpringApplication.run(Summative4Application.class, args);
	}

}

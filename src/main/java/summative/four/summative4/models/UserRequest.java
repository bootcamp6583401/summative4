package summative.four.summative4.models;

import org.springframework.aot.generate.Generated;

import lombok.Getter;
import lombok.Setter;

@Generated
public class UserRequest {
    
    @Getter
    @Setter
    private User user;
    
    @Getter
    @Setter
    private String department;


}

package summative.four.summative4.models;


import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @NotNull(message = "Name cannot be empty")
    private String name;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    @Email(message = "Email must be an email")
    @NotNull(message = "Email cannot be empty")
    private String email;

    @Getter
    @Setter
    @NotNull(message = "Birth date cannot be empty")
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    @JsonBackReference
    @Getter
    @Setter
    private Department department;

    public String getDepartmentName(){
        if(this.getDepartment() == null) return "Unemployed";
        return this.getDepartment().getName();
    }


}


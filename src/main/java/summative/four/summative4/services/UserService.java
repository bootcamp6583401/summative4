package summative.four.summative4.services;



import java.util.List;


import summative.four.summative4.models.User;



public interface UserService {
    List<User> getAll();

    User getById(int id);

    User save(User obj);

    void deleteById(int id);

    User updateById(int id, User brand);

     List<User> saveMany(Iterable<User> users);

}

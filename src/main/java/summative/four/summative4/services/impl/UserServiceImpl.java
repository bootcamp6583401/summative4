package summative.four.summative4.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.four.summative4.models.User;
import summative.four.summative4.repositories.UserRepository;
import summative.four.summative4.services.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repo;

    public List<User> getAll() {
        return repo.findAll();
    }

    public User save(User comment) {
        return repo.save(comment);
    }

    public User getById(int id) {
        Optional<User> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public User updateById(int id, User comment) {

        comment.setId(id);
        return repo.save(comment);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public List<User> saveMany(Iterable<User> newUsers){
        Iterable<User> comments = repo.saveAll(newUsers);
        return StreamSupport.stream(comments.spliterator(), false)
                .collect(Collectors.toList());
    }

}
package summative.four.summative4.services.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import summative.four.summative4.models.Department;
import summative.four.summative4.repositories.DepartmentRepository;
import summative.four.summative4.services.DepartmentService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    DepartmentRepository repo;

    public List<Department> getAll() {
        return repo.findAll();
    }

    public Department save(Department comment) {
        return repo.save(comment);
    }

    public Department getById(int id) {
        Optional<Department> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Department updateById(int id, Department comment) {

        comment.setId(id);
        return repo.save(comment);

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }

    public List<Department> saveMany(Iterable<Department> newDepartments){
        Iterable<Department> comments = repo.saveAll(newDepartments);
        return StreamSupport.stream(comments.spliterator(), false)
                .collect(Collectors.toList());
    }

    public Optional<Department> findByName(String name) {
        return repo.findByName(name);
    }

}
package summative.four.summative4.services;



import java.util.List;
import java.util.Optional;

import summative.four.summative4.models.Department;



public interface DepartmentService {
    List<Department> getAll();

    Department getById(int id);

    Department save(Department obj);

    void deleteById(int id);

    Department updateById(int id, Department brand);

     List<Department> saveMany(Iterable<Department> users);

     Optional<Department> findByName(String name);

}

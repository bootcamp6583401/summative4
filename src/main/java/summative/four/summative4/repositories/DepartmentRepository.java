package summative.four.summative4.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import summative.four.summative4.models.Department;



public interface DepartmentRepository extends CrudRepository<Department, Integer> {

    @Query(value ="SELECT * FROM department", nativeQuery = true)
    List<Department> findAll();

    Optional<Department> findByName(String name);
}

package summative.four.summative4.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import summative.four.summative4.models.User;



public interface UserRepository extends CrudRepository<User, Integer> {

    @Query(value ="SELECT * FROM user", nativeQuery = true)
    List<User> findAll();
}

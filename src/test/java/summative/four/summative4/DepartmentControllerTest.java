package summative.four.summative4;



import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import summative.four.summative4.controllers.DepartmentController;
import summative.four.summative4.models.Department;
import summative.four.summative4.models.User;
import summative.four.summative4.services.DepartmentService;

import java.util.List;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DepartmentController.class)
public class DepartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService service;

    @Test
    public void getAllDepartments_thenReturnJsonArray() throws Exception {
        Department department1 = new Department();
        department1.setId(1);
        department1.setName("Department1");

        Department department2 = new Department();
        department2.setId(2);
        department2.setName("Department2");

        User user1 = new User();
        user1.setId(1);
        user1.setName("Name");
        user1.setLastName("Last");
        user1.setBirthDate(LocalDate.now());
        user1.setEmail("email@email.com");

        department1.setUsers(Arrays.asList(user1));


        List<Department> allDepartments = Arrays.asList(department1, department2);

        given(service.getAll()).willReturn(allDepartments);

        mockMvc.perform(get("/departments")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(department1.getName())))
                .andExpect(jsonPath("$[0].users[0].name", is(user1.getName())))
                .andExpect(jsonPath("$[1].name", is(department2.getName())));
    }

    @Test
    public void getDepartmentById_WhenDepartmentExists() throws Exception {
        int departmentId = 1;
        Department department = new Department();
        department.setId(departmentId);
        department.setName("Department1");

        given(service.getById(departmentId)).willReturn(department);

        mockMvc.perform(get("/departments/{id}", departmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())));
    }

    @Test
    public void createDepartment_WhenPostDepartment() throws Exception {
        Department department = new Department();
        department.setId(1);
        department.setName("Department1");

        given(service.save(any(Department.class))).willReturn(department);

        mockMvc.perform(post("/departments")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Department1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())));
    }

    
    @Test
    public void saveMany() throws Exception {
        Department department1 = new Department();
        department1.setId(1);
        department1.setName("Department1");

        Department department2 = new Department();
        department2.setId(1);
        department2.setName("Department2");

        given(service.saveMany(anyList())).willReturn(Arrays.asList(department1, department2));

        mockMvc.perform(post("/departments/many")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{\"name\":\"Department1\"}, {\"name\": \"Department2\"}]"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(department1.getName())))
                .andExpect(jsonPath("$[1].name", is(department2.getName())));

    }

    @Test
    public void deleteDepartment_WhenDepartmentExists() throws Exception {
        int departmentId = 1;

        doNothing().when(service).deleteById(departmentId);

        mockMvc.perform(delete("/departments/{id}", departmentId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateDepartment_WhenPutDepartment() throws Exception {
        int departmentId = 1;
        Department department = new Department();
        department.setId(departmentId);
        department.setName("Updated Department");

        given(service.updateById(eq(departmentId), any(Department.class))).willReturn(department);

        mockMvc.perform(put("/departments/{id}", departmentId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Department\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(department.getName())));
    }
}
package summative.four.summative4;



import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import summative.four.summative4.controllers.UserController;
import summative.four.summative4.models.Department;
import summative.four.summative4.models.User;
import summative.four.summative4.services.DepartmentService;
import summative.four.summative4.services.UserService;

import java.util.List;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @MockBean
    private DepartmentService depService;

    @Test
    public void getAllUsers_thenReturnJsonArray() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setName("User1");

        User user2 = new User();
        user2.setId(2);
        user2.setName("User2");

        Department department = new Department();
        department.setId(1);
        department.setName("Dep");

        user1.setDepartment(department);
        user2.setDepartment(department);


        List<User> allUsers = Arrays.asList(user1, user2);

        given(service.getAll()).willReturn(allUsers);

        mockMvc.perform(get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(user1.getName())))
                .andExpect(jsonPath("$[1].name", is(user2.getName())));
    }

    @Test
    public void getUserById_WhenUserExists() throws Exception {
        int userId = 1;
        User user = new User();
        user.setId(userId);
        user.setName("Name");
        user.setLastName("Last");
        user.setBirthDate(LocalDate.now());
        user.setEmail("email@email.com");

        Department dep = new Department();
        dep.setId(1);
        dep.setName("Dep");

        user.setDepartment(dep);

        given(service.getById(userId)).willReturn(user);

        mockMvc.perform(get("/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user.getName())))
                .andExpect(jsonPath("$.lastName", is(user.getLastName())))
                .andExpect(jsonPath("$.birthDate", is(user.getBirthDate().toString())))
                .andExpect(jsonPath("$.id", is(user.getId())))
                .andExpect(jsonPath("$.email", is(user.getEmail())))
                .andExpect(jsonPath("$.departmentName", is(user.getDepartmentName())));
    }

    @Test
    public void createUser_WhenPostUser() throws Exception {
        User user = new User();
        user.setId(1);
        user.setName("User1");

        given(service.save(any(User.class))).willReturn(user);

        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"User1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user.getName())));
    }

    @Test
    public void saveMany() throws Exception {
        User user1 = new User();
        user1.setId(1);
        user1.setName("User1");
        user1.setEmail("email@email.com");
        user1.setBirthDate(LocalDate.now());
        user1.setLastName("Last");

        User user2 = new User();
        user2.setId(1);
        user2.setName("User2");
        user2.setEmail("email@email.com");
        user2.setBirthDate(LocalDate.now());
        user2.setLastName("Last");

        given(service.saveMany(anyList())).willReturn(Arrays.asList(user1, user2)); 

        mockMvc.perform(post("/users/many")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[{\"name\":\"User1\", \"lastName\": \"Last\", \"email\": \"email@email.com\", \"birthDate\": \"2023-01-01\"}, {\"name\": \"User2\", \"lastName\": \"Last\", \"email\": \"email@email.com\", \"birthDate\": \"2023-01-01\"}]"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is(user1.getName())))
                .andExpect(jsonPath("$[1].name", is(user2.getName())));

    }


    @Test
    public void deleteUser_WhenUserExists() throws Exception {
        int userId = 1;

        doNothing().when(service).deleteById(userId);

        mockMvc.perform(delete("/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateUser_WhenPutUser() throws Exception {
        int userId = 1;
        User user = new User();
        user.setId(userId);
        user.setName("Updated User");

        given(service.updateById(eq(userId), any(User.class))).willReturn(user);

        mockMvc.perform(put("/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated User\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(user.getName())));
    }
}
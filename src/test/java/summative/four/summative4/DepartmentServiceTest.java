package summative.four.summative4;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import summative.four.summative4.models.Department;
import summative.four.summative4.repositories.DepartmentRepository;
import summative.four.summative4.services.impl.DepartmentServiceImpl;



@SpringBootTest
public class DepartmentServiceTest {
    @Mock
    private DepartmentRepository repo;

    @InjectMocks
    private DepartmentServiceImpl service;

    @Test
    public void whenGetAllDepartments_thenReturnDepartmentList() {
        Department obj = new Department();
        obj.setId(1);
        obj.setName("Test Department");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Department> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Department", result.get(0).getName());
    }

    @Test
    public void whenGetDepartmentById_thenReturnDepartment() {
        Department obj = new Department();
        obj.setId(1);
        obj.setName("Test Department");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Department result = service.getById(1);

        assertEquals("Test Department", result.getName());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        Department result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSaveDepartment_thenDepartmentShouldBeSaved() {
        Department obj = new Department();
        obj.setId(1);
        obj.setName("Test Department");

        when(repo.save(any(Department.class))).thenReturn(obj);

        Department savedDepartment = service.save(obj);

        assertNotNull(savedDepartment);
        assertEquals(obj.getName(), savedDepartment.getName());
        verify(repo, times(1)).save(obj);
    }

    @Test
    public void savemany() {
        Department obj1 = new Department();
        obj1.setId(1);
        obj1.setName("Test Department1");

        Department obj2 = new Department();
        obj2.setId(2);
        obj2.setName("Test Department2");

        when(repo.saveAll(anyList())).thenReturn(Arrays.asList(obj1, obj2));

        List<Department> savedObjs = service.saveMany(Arrays.asList(obj1, obj2));

        assertNotNull(savedObjs);
        assertEquals(obj1.getName(), savedObjs.get(0).getName());
        assertEquals(obj2.getName(), savedObjs.get(1).getName());
        verify(repo, times(1)).saveAll(Arrays.asList(obj1, obj2));
    }


    @Test
    public void whenDeleteDepartment_thenDepartmentShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateDepartment_thenDepartmentShouldBeUpdated() {
        Department existingDepartment = new Department();
        existingDepartment.setId(1);
        existingDepartment.setName("Old Department Name");

        Department updated = new Department();
        updated.setId(1);
        updated.setName("Updated Department Name");

        when(repo.findById(existingDepartment.getId())).thenReturn(Optional.of(existingDepartment));
        when(repo.save(any(Department.class))).thenAnswer(i -> i.getArguments()[0]);

        Department updatedDepartment = service.updateById(existingDepartment.getId(), updated);

        assertNotNull(updatedDepartment);
        assertEquals(updated.getName(), updatedDepartment.getName());
        verify(repo, times(1)).save(updatedDepartment);
    }

}

